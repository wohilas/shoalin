#!/bin/bash

echo started

python3 manage.py makemigrations
python3 manage.py makemigrations api
python3 manage.py migrate --no-input

echo "finished migrations"

# creates admin if doesnt exists yet
#python3 backend/api/management/create_admin.py
python3 manage.py initadmin

while true; do
    python3 manage.py runserver 0.0.0.0:8000
    sleep 5s
done