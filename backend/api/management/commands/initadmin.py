from django.contrib.auth.models import User
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Creates a default superuser for local development'

    def handle(self, *args, **options):
        if not User.objects.all():
            print("Creating default user")
            User.objects.create_superuser(
                username="admin",
                email='admin@example.com',
                password='password'
            )
            print(
                """
                Default user created:
                email: 'admin@example.com'
                password: 'password'
                """)
        else:
            print("Not creating default user; already exists")
