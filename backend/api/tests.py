from django.test import TestCase
import uuid

# Create your tests here.
class LinkTest(TestCase):
    
    def test_make_link(self):
        post_body = {
            "user_id": uuid.uuid4(),
            "full_link": "http://example.com/somepage"
        }
        response = self.client.post('/', data=post_body)
        self.assertEqual(response.status_code, 302)

        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertIn(post_body["full_link"], response.data[0].values())