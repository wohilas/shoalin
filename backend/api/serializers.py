
from rest_framework.serializers import ModelSerializer
from api.models import ShortLink

class ShortLinkSerializer(ModelSerializer):

    class Meta:
        model = ShortLink
        fields = ["full_link", "small_link", "user_id", "subpart"]

class ShortLinkListSerializer(ModelSerializer):

    class Meta:
        model = ShortLink
        fields = ["full_link", "small_link"]

class ShortLinkPostSerializer(ModelSerializer):

    class Meta:
        model = ShortLink
        fields = ["full_link", "user_id", "subpart"]