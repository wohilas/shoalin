from django.db import models
import uuid
# Create your models here.

class ShortLink(models.Model):
    id = models.SlugField(max_length=6,primary_key=True)
    user_id = models.UUIDField(null=False)
    full_link = models.URLField()
    small_link = models.URLField()
    subpart = models.CharField(max_length=6, null=True)

    def serialize(self):
        return {
            "full_link": self.full_link,
            "small_link": self.small_link,
        }

