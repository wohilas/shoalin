from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse
# Create your views here.
from api.models import ShortLink
from api.serializers import ShortLinkSerializer, ShortLinkListSerializer, ShortLinkPostSerializer
from rest_framework import mixins
from rest_framework import generics
import string, random, os, urllib.parse as urlparse
import json

class ShortLinkList(mixins.ListModelMixin,
                  mixins.CreateModelMixin,
                  generics.GenericAPIView):
    queryset = ShortLink.objects.all()
    serializer_class = ShortLinkSerializer

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.serializer_class = ShortLinkPostSerializer
        print(request.data)
        # split link on domain and subpart
        # check wish-subpart
        full_link = request.data.get("full_link", '')
        user_id = request.data.get("user_id", '')
        subpart = request.data.get("subpart")
        if not (full_link == ''):

            # check on full_link

            short_id = get_short_code(subpart=subpart)
            print(short_id)
            url = ShortLink(full_link=full_link, id=short_id, user_id=user_id)
            # get small_url
            base_url = os.environ.get("BASE_URL", "http://localhost:8000/")
            url.small_link = urlparse.urljoin(base_url, short_id)
            url.save()


            # session cache part
            # user_id is key, list of links is value
            user_li = request.session.get(user_id, [])
            user_li.append(url.serialize())

            if not user_id in request.session:
                request.session[user_id] = user_li
            request.session.save()
            return HttpResponseRedirect(reverse("list"))

        # gets here only if full_link isnt provided, so url wouldn't be created
        return self.create(request, *args, **kwargs)


def redirect_original(request, short_id):
    url = get_object_or_404(ShortLink, pk=short_id)
    return HttpResponseRedirect(url.full_link)

def user_links(request, user_id):
    # user_id is type uuid, convert to str
    user_li = request.session.get(str(user_id), [])
    response = HttpResponse(json.dumps(user_li))
    return response

def get_short_code(subpart=None):
    length = 6
    char = string.ascii_uppercase + string.digits + string.ascii_lowercase
    # if the randomly generated short_id is used then generate next

    while True:

        if subpart:
            short_id = subpart
        else:
            short_id = ''.join(random.choice(char) for x in range(length))
        try:
            _ = ShortLink.objects.get(pk=short_id)
        except:
            subpart = None
            return short_id