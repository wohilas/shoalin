## LINK SHORTENER

#### Shoalin (short a link)?

```
docker-compose up -d --build
```


### Methods:

- POST '/' => creates ShortLink instance ![ShortLink instance](closet/shortlink.png "ShortLink instance") using fields:
![ShortLink fields](closet/shortlink_fields.png "ShortLink fields")
- GET '/{user_id:UUID}' => gets list of dictionaries: [{"full_link": f_link, "small_link": s_link}]
![get_result instance](closet/get_result.png "get_result instance")
- GET '/' = > returns full list of ![ShortLink instance](closet/shortlink.png "ShortLink instance")

### Watching User practice
frontend generates uuid and keeps it in local storage;  
backend uses that uuid as key for links of current user.